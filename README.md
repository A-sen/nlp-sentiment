##基于自然语言处理的情感分析工具

## 请注意：
>1. pom.xml 文件中  lingpipe-core jar 没办法从中央仓库下载已经集成到 libs 目录里面，而 pom.xml 中只能使用绝对路径，请注意修改
>2. pom.xml 文件中  hanlp jar 包可能需要手动下载，然后添加到 maven 仓库

### 本程序依赖data目录下面的data.zip和dictionary.zip先解压缩  data 目录下面的 data.zip到当前目录。

###	设计思想
专注某一个领域，比如军事、经济、政府等，利用互联网上的数据。
>1. 首先训练一个专业领域与非专业其它领域的二分类器。
>2. 然后在对新的样本判断是否是该领域的
>3. 训练一个情感分类器，判断这个样本是该领域的正面信息还是负面信息。


###	新增说明4：将模型训练和模型生成应用分离，提炼一些测试用例。
1. 新增 NGramClassierTrainer 用于基于 NGram 特征的分类器训练
2. 增加模型训练配置类：ClassModelConfiguration

###	新增说明3：增加基于 TF-IDF(词向量) 特征的文本分类程序。
1. 主程序：DfIdfClassifier.java
2. 效果如下：

+ CATEGORY      nment   others
+ government     233      46
+ others         110     390
+ 准确度: 0.8
+ 总共正确数 : 623
+ 总数：779

###	新增说明2：增加基于 N-Gram(词向量) 特征的文本分类程序，目的是找出自己领域相关的文本，然后再从这个领域相关的文本中判断正负面。

1. 测试语料：data/text_classification.zip 解压缩即可
2. 运行程序：NGramClassifier.java 即可。
3. 效果如下：

+ Total Accuracy=0.9550706033376123
+ 95% Confidence Interval=0.9550706033376123 +/- 0.014546897368198444
+ Confusion Matrix
+ reference \ response
+   			 government,others
+   government 271,			8
+   others	 27,			473

###	新增说明1：2015-04-10测试了不用中文分词器，分词之后 LingPipe 情感分类的准确率，同时测试了去除停用词之后的情感分类的准确率。
####注意：有时候不用中文分词器效果更好，一定要测试。

1. 发现用HanLP的NLPTokenizer分词器，准确率最高，但是速度有点慢。
2. 如果用HanLP的标准分词器就会准确率低一点点，但是速度快。
3. 分词之后去除停用词效果更加差。
4. 结巴分词效果不好，而且速度慢。

###1、基于词典和贝叶斯模型的情感分析
主程序：eshore.cn.it.sentiment.Sentiment 此类通过
data/Sentiment_Dictionary中的正负面词语建立模型。

测试： eshore.cn.it.sentiment.SentimentTest
通过这个类就可以测试 data/500trainblogxml中的某个文件夹下面的博客的情感。

###2、直接利用lingpipe的情感分析模块测试情感分析
直接运行程序：  eshore.cn.it.sentiment.ChinesePolarityBasic
程序就会通过：  data/polarity_corpus/hotel_reviews/train2训练
然后自动测试: data/polarity_corpus/hotel_reviews/test2
最后给出程序测试结果。

```
  # Test Cases=4000
  # Correct=3541
  % Correct=0.88525
```